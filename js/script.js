window.addEventListener("DOMContentLoaded", ()=>{

  const data = new XMLHttpRequest();
  data.open("GET", "https://swapi.dev/api/people");
  data.send();

  const spinner = document.querySelector(".spinner-grow"),
  pagination = document.querySelector("nav");
  spinner.style.display = "block";
  pagination.style.display = "none";

  data.onreadystatechange = ()=> {
      if(data.readyState === 4 && data.status === 200){
        pagination.style.display = "block";
        show(JSON.parse(data.responseText).results);
        spinner.style.display = "none";
        console.log(JSON.parse(data.responseText));
      }
    }

  function show(data_info){
    data_info.forEach((el) => {
      const section = document.querySelector(".section"),
      div = document.createElement('div'),
      id = `div${Math.random() * 100000}`;
      div.style.maxWidth = "18rem";
      div.classList.add("card","border-danger","mb-3");
      div.id = id;

      const h1 = document.createElement("h1");
      h1.innerText = el.name;
      h1.classList.add("card-header");

      const div_body = document.createElement("div");
      div_body.classList.add("card-body", "text-danger");

      const h5 = document.createElement("div");
      h5.classList.add("card-title");
      h5.innerText = el.gender;

      const p = document.createElement('p');
      p.classList.add("card-text");

      const planet = new XMLHttpRequest();
      planet.open("GET", el.homeworld)
      planet.send();
      planet.onreadystatechange = ()=>{
        if(planet.readyState === 4 && planet.status === 200){
          let homeworldName = JSON.parse(planet.responseText);
          p.innerText = homeworldName.name;
        }
      }

      const p2 = document.createElement('p');

      const information = new XMLHttpRequest();
      information.open("GET", el.url);
      information.send();

      information.onreadystatechange = ()=>{
        if(information.readyState === 4 && information.status === 200){
          let allInformation = JSON.parse(information.responseText);
          p2.innerText = `name: ${allInformation.name};
          height: ${allInformation.height}; 
          mass: ${allInformation.mass};
          hair_color: ${allInformation.hair_color};
          skin_color: ${allInformation.skin_color};
          gender: ${allInformation.gender}; 
          eye_color: ${allInformation.eye_color}.`;

          p2.style.display = "none";
          
        }
      }
      const p3 = document.createElement("p");

        let infoFilms = el.films
        infoFilms.forEach((link)=>{
          const film = new XMLHttpRequest();
          film.open("GET", link);
          film.send();
          film.onreadystatechange = ()=>{
            if(film.readyState === 4 && film.status === 200){
              let linkFilm = JSON.parse(film.responseText);
              p3.innerText += `"${linkFilm.title}", `;
              p3.style.display = "none";

              const btn = document.getElementById(id);
              let amount = 0;
    
              btn.addEventListener('click', function(){
                if(amount === 0){
                  p2.style.display = "block";
                  p3.style.display = "block";
                  h5.style.display = "none";
                  p.style.display = "none";
                  amount++;
                }else{
                  p2.style.display = "none";
                  p3.style.display = "none";
                  h5.style.display = "block";
                  p.style.display = "block";
                  amount = 0;
                }
              })
            }
          }
        })
        section.append(div);
        div.append(h1,div_body);
        div_body.append(h5,p,p2,p3);
    })
  }
  Array.from(document.querySelectorAll(".page-link")).forEach((el)=>{
    const id = `a${Math.random() * 36000}`;
    el.id = id;
    
    const btn = document.getElementById(id);
    btn.addEventListener('click', (e)=>{
      spinner.style.display = "block";
      document.querySelector('.section').innerText = '';
      let num = e.target.innerText;

        const next = new XMLHttpRequest();
        next.open("GET", `https://swapi.dev/api/people/?page=${num}`)
        next.send();

        next.onreadystatechange = ()=>{
          if(next.readyState === 4 && next.status === 200){
            show(JSON.parse(next.responseText).results);
            spinner.style.display = "none";
          } 
        }
      })
    })
  })